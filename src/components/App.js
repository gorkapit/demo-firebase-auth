import React from 'react'
import styled, { injectGlobal } from 'styled-components'
import FirebaseAuth from './FirebaseAuth'

injectGlobal`
  body{
    margin: 0;
    font: 13px Menlo, Monaco, Lucida Console, Liberation Mono, DejaVu Sans Mono, Bitstream Vera Sans Mono, Courier New, monospace, serif;
  }
`;

const Content = styled.div`
  display: flex;
  flex-flow: column wrap;
  height: 100vh;
  box-sizing: border-box;
`;

const AppDemo = styled.div`
  flex: 1;
  padding: 1em;
`;

const Footer = styled.div`
  color: white;
  background-color: #9C3C86;

  p{
    margin: 1em;
  }

  a{
    color: white;
  }
`;

const App = () => {
  return (
    <Content>
      <AppDemo>
        <FirebaseAuth />
      </AppDemo>
      <Footer>
        <p>Ejemplo de autenticación con Facebook usando React y Firebase. Encuentra el código en <a href="http://gorkapit.com">gorkapit.com</a></p>
      </Footer>
    </Content>
  )
}

export default App
