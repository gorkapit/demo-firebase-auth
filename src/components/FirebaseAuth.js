import React, { Component } from 'react'

import * as firebase from 'firebase'

const firebaseConfig = {
  apiKey: "AIzaSyBBPBeRi6TVnWVvQEeEr_5PVqD0iLcgoMc",
  authDomain: "fir-apps-488f2.firebaseapp.com"
}

firebase.initializeApp(firebaseConfig)
const provider = new firebase.auth.FacebookAuthProvider()

const Button = ({ text, onClick }) => (
  <button onClick={onClick}>
    {text}
  </button>
)

class FirebaseAuth extends Component {
  constructor(props) {
    super(props)

    this.state = {
      user: null
    }
  }

  componentWillMount() {
    firebase.auth().onAuthStateChanged(user => {
      if (user) {
        this.setState({ user })
      } else {
        this.setState({ user: null })
      }
    })
  }

  logIn() {
    firebase.auth().signInWithPopup(provider)
  }

  logOut() {
    firebase.auth().signOut()
  }

  render() {
    if (this.state.user) {
      return (
        <div>
          <p>{this.state.user.displayName}</p>
          <p><img src={this.state.user.photoURL} alt={this.state.user.displayName} /></p>
          <Button text="Desconectar" onClick={this.logOut} />
        </div>
      )
    } else {
      return <Button text="Entra con Facebook" onClick={this.logIn} />
    }
  }
}

export default FirebaseAuth
